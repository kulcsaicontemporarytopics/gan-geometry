# Generate basic shapes using GANs
## Introduction
This repository uses the [Baby AI Shapes Dataset](https://www.iro.umontreal.ca/~lisa/twiki/bin/view.cgi/Public/BabyAIShapesDatasets) to train a basic shape generator using Generative Adversarial Networks (GANs). 
The training code is based on [tensorflow/models/research/gan](https://github.com/tensorflow/models/tree/master/research/gan).

## Data generation
* Create a python2 virtual environment: `virtualenv -p python2 ~/gans-py2-venv`.
* Activate the python2 virtual environment: `source ~/gans-py2-venv/bin/activate`.
* Move to the BabyAIShapesDatasets directory: `cd BabyAIShapesDatasets`
* Install the requirements for data generation: `pip install -r ./requirements.txt`
* Generate datasets:
    * Either generate single datasets: 
`cd shapeset && python shapeset1_1cspo_2_3.5000.test.py write_formats amat && mv ./shapeset1_1cspo_2_3.5000.test.amat ../../generated-data/` (obviously you can substitute the dataset name).
    * Or generate all datasets at once (Assuming you have enough time and disk space): `./generate-all-datasets.sh ../generated-data`

## Train the shape generator
* Create a python3 virtual environment: `virtualenv -p python3 ~/gans-py3-venv`.
* Activate the python3 virtual environment: `source ~/gans-py3-venv/bin/activate`.
* Install the requirements for training the shape generator: `pip install -r requirements.txt`. If you have a GPU, you might want to install TensorFlow-GPU instead of the regular TensorFlow (`pip install tensorflow-gpu`).
* Start Jupyter Notebook: `jupyter notebook` and open `TFGAN-SHAPES.ipynb`.

## Running on remote machine with SSH Tunnel
* On the remote machine (accessible at `XXX`), create the data and run the notebook as above, except that you prevent jupyter from opening a browser: the `jupyter notebook --no-browser`.
* In a terminal on your client machine, set up an SSH tunnel: `ssh XXX -L 8888:localhost:8888 -N`.
* Copy-paste the URL with the one-time login token printed when starting jupyter into a local browser.