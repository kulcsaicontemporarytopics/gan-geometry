#!/bin/bash


TARGET_DIRECTORY=".."
if [ $# -ge 1 ]; then
    TARGET_DIRECTORY="$1"
fi

echo $TARGET_DIRECTORY
mkdir -p $TARGET_DIRECTORY

cd shapeset || exit 1
for i in ./shapeset*.py; do
    python2 $i write_formats amat || exit 1
    new_filename="${i%.py}.amat"
done

cd .. || exit 1
for i in ./shapeset/shapeset*.amat; do
    echo $i
    new_filename="${TARGET_DIRECTORY}/${i#./shapeset}"
    mv $i "${new_filename}"
done

echo "Done writing datasets"