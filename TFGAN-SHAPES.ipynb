{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# TFGAN Shapes\n",
    "\n",
    "## Authors: Floris Kint, Rugen Heidbuchel\n",
    "\n",
    "### Adapted from [`tensorflow/models/gan`](https://github.com/tensorflow/models/tree/master/research/gan)\n",
    "\n",
    "\n",
    "This notebook will walk you through our experiment of GANs for the generation of basic shapes.\n",
    "\n",
    "Please note that running on GPU will significantly speed up the training steps, but is not required."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1>Table of Contents</h1>\n",
    "<ul>\n",
    "    <li>\n",
    "        <a href=\"#installation_and_setup\">Installation and setup</a>\n",
    "        <ul>\n",
    "            <li><a href=\"#imports\">Imports</a></li>\n",
    "            <li><a href=\"#common_functions\">Common functions</a></li>\n",
    "            <li><a href=\"#parameters\">Parameters</a></li>\n",
    "            <li><a href=\"#dataset\">Setup dataset</a></li>\n",
    "        </ul>\n",
    "    </li>\n",
    "    <li>\n",
    "        <a href=\"#unconditional_gan\">Unconditional GAN</a>\n",
    "        <ul>\n",
    "            <li>\n",
    "                <a href=\"#unconditional_model\">Model</a>\n",
    "                <ul>\n",
    "                    <li><a href=\"#unconditional_generator\">Generator</a></li>\n",
    "                    <li><a href=\"#unconditional_discriminator\">Discriminator</a></li>\n",
    "                    <li><a href=\"#unconditional_tuple\">GANModel Tuple</a></li>\n",
    "                </ul>\n",
    "            </li>\n",
    "            <li><a href=\"#unconditional_losses\">Losses</a></li>\n",
    "            <li>\n",
    "                <a href=\"#unconditional_train\">Training</a>\n",
    "                <ul>\n",
    "                    <li><a href=\"#unconditional_train_ops\">Train ops</a></li>\n",
    "                    <li><a href=\"#unconditional_train_steps\">Train steps</a></li>\n",
    "                </ul>\n",
    "            </li>\n",
    "        </ul>\n",
    "    </li>\n",
    "    <li>\n",
    "        <a href=\"#conditional_gan\">Conditional GAN</a>\n",
    "        <ul>\n",
    "            <li>\n",
    "                <a href=\"#conditional_model\">Model</a>\n",
    "                <ul>\n",
    "                    <li><a href=\"#conditional_generator\">Generator</a></li>\n",
    "                    <li><a href=\"#conditional_discriminator\">Discriminator</a></li>\n",
    "                    <li><a href=\"#conditional_tuple\">GANModel Tuple</a></li>\n",
    "                </ul>\n",
    "            </li>\n",
    "            <li><a href=\"#conditional_losses\">Losses</a></li>\n",
    "            <li>\n",
    "                <a href=\"#conditional_train\">Training</a>\n",
    "                <ul>\n",
    "                    <li><a href=\"#conditional_train_ops\">Train ops</a></li>\n",
    "                    <li><a href=\"#conditional_train_evaluation\">Evaluation</a></li>\n",
    "                    <li><a href=\"#conditional_train_steps\">Train steps</a></li>\n",
    "                </ul>\n",
    "            </li>\n",
    "        </ul>\n",
    "    </li>\n",
    "</ul>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1><a id=\"installation_and_setup\"></a>Installation and setup</h1>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h2><a id='imports'></a>Imports</h2>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from __future__ import absolute_import\n",
    "from __future__ import division\n",
    "from __future__ import print_function\n",
    "\n",
    "import os\n",
    "\n",
    "%matplotlib inline\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import time\n",
    "import functools\n",
    "\n",
    "import tensorflow as tf\n",
    "\n",
    "# Main TFGAN library.\n",
    "tfgan = tf.contrib.gan\n",
    "\n",
    "# Shortcuts for later.\n",
    "slim = tf.contrib.slim\n",
    "layers = tf.contrib.layers\n",
    "ds = tf.contrib.distributions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h2><a id='common_functions'></a>Common functions</h2>\n",
    "These functions are used by many examples, so we define them here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "leaky_relu = lambda net: tf.nn.leaky_relu(net, alpha=0.01)\n",
    "  \n",
    "\n",
    "def visualize_training_generator(train_step_num, start_time, data_np):\n",
    "    \"\"\"Visualize generator outputs during training.\n",
    "    \n",
    "    Args:\n",
    "        train_step_num: The training step number. A python integer.\n",
    "        start_time: Time when training started. The output of `time.time()`. A\n",
    "            python float.\n",
    "        data: Data to plot. A numpy array, most likely from an evaluated TensorFlow\n",
    "            tensor.\n",
    "    \"\"\"\n",
    "    print('Training step: %i' % train_step_num)\n",
    "    time_since_start = (time.time() - start_time) / 60.0\n",
    "    print('Time since start: %f m' % time_since_start)\n",
    "    print('Steps per min: %f' % (train_step_num / time_since_start))\n",
    "    plt.axis('off')\n",
    "    plt.imshow(np.squeeze(data_np), cmap='gray')\n",
    "    plt.show()\n",
    "\n",
    "def visualize_images(tensor_to_visualize):\n",
    "    \"\"\"Visualize an image once. Used to visualize generator before training.\n",
    "    \n",
    "    Args:\n",
    "        tensor_to_visualize: An image tensor to visualize. A python Tensor.\n",
    "    \"\"\"\n",
    "    with tf.Session() as sess:\n",
    "        sess.run(tf.global_variables_initializer())\n",
    "        with slim.queues.QueueRunners(sess):\n",
    "            images_np = sess.run(tensor_to_visualize)\n",
    "    plt.axis('off')\n",
    "    plt.imshow(np.squeeze(images_np), cmap='gray')\n",
    "\n",
    "def evaluate_tfgan_loss(gan_loss, name=None):\n",
    "    \"\"\"Evaluate GAN losses. Used to check that the graph is correct.\n",
    "    \n",
    "    Args:\n",
    "        gan_loss: A GANLoss tuple.\n",
    "        name: Optional. If present, append to debug output.\n",
    "    \"\"\"\n",
    "    with tf.Session() as sess:\n",
    "        sess.run(tf.global_variables_initializer())\n",
    "        with slim.queues.QueueRunners(sess):\n",
    "            gen_loss_np = sess.run(gan_loss.generator_loss)\n",
    "            dis_loss_np = sess.run(gan_loss.discriminator_loss)\n",
    "    if name:\n",
    "        print('%s generator loss: %f' % (name, gen_loss_np))\n",
    "        print('%s discriminator loss: %f'% (name, dis_loss_np))\n",
    "    else:\n",
    "        print('Generator loss: %f' % gen_loss_np)\n",
    "        print('Discriminator loss: %f'% dis_loss_np)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h2><a id='parameters'></a>GAN parameters</h2>\n",
    "These constants identify some of the parameters of the algorithm. You can change them to experiment with the results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "BATCH_SIZE = 32\n",
    "GENERATOR_STEPS = 1\n",
    "DISCRIMINATOR_STEPS = 1\n",
    "NR_NOISE_DIMENSIONS = 64\n",
    "REPEATS = 10\n",
    "FILENAME_PREFIX = \"./generated-data\"\n",
    "ALL_FILENAMES = {\n",
    "    \"20000-symmetric-all-fixed\": {\n",
    "        \"filenames\": [\n",
    "            \"shapeset1_1cs_2p_3o.5000.valid.amat\",\n",
    "            \"shapeset1_1cs_2p_3o.5000.test.amat\",\n",
    "            \"shapeset1_1cs_2p_3o.10000.train.amat\"\n",
    "        ], \n",
    "        \"filter\": None\n",
    "    },\n",
    "    \"20000-variable-all-variable\": {\n",
    "        \"filenames\":[\n",
    "            \"shapeset2_1cspo_2_3.10000.train.amat\",\n",
    "            \"shapeset2_1cspo_2_3.5000.test.amat\",\n",
    "            \"shapeset2_1cspo_2_3.5000.valid.amat\"\n",
    "        ],\n",
    "        \"filter\": None\n",
    "    },\n",
    "    \"20000-symmetric-all-variable\": {\n",
    "        \"filenames\":[\n",
    "            \"shapeset1_1cspo_2_3.10000.train.amat\",\n",
    "            \"shapeset1_1cspo_2_3.5000.test.amat\",\n",
    "            \"shapeset1_1cspo_2_3.5000.valid.amat\"\n",
    "        ],\n",
    "        \"filter\": None\n",
    "    },\n",
    "    \"20000-variable-all-fixed\": {\n",
    "        \"filenames\":[\n",
    "            \"shapeset2_1csp_2o_3.10000.train.amat\",\n",
    "            \"shapeset2_1csp_2o_3.5000.test.amat\",\n",
    "            \"shapeset2_1csp_2o_3.5000.valid.amat\"\n",
    "        ],\n",
    "        \"filter\": None\n",
    "    },\n",
    "    \"10000-symmetric-all-fixed\": {\n",
    "        \"filenames\":[\n",
    "            \"shapeset1_1cs_2p_3o.10000.train.amat\",\n",
    "        ],\n",
    "        \"filter\": None\n",
    "    },\n",
    "    \"6500-symmetric-triangle-fixed\": {\n",
    "        \"filenames\":[\n",
    "            \"shapeset1_1cs_2p_3o.5000.valid.amat\",\n",
    "            \"shapeset1_1cs_2p_3o.5000.test.amat\",\n",
    "            \"shapeset1_1cs_2p_3o.10000.train.amat\"\n",
    "        ],\n",
    "        \"filter\": \"1\"\n",
    "    }\n",
    "}\n",
    "DATASET_TYPE = \"20000-symmetric-all-fixed\"\n",
    "FILENAMES = [os.path.join(FILENAME_PREFIX, x) for x in ALL_FILENAMES[DATASET_TYPE][\"filenames\"]]\n",
    "SHAPE_FILTER = ALL_FILENAMES[DATASET_TYPE][\"filter\"] # None, \"1\", \"2\" or \"3\"\n",
    "NR_SHAPES = 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h2><a id='dataset'></a>Setup dataset</h2>\n",
    "The <a href=\"www.iro.umontreal.ca/~lisa/twiki/bin/view.cgi/Public/BabyAIShapesDatasets\">Baby AI Shape Datasets</a> is used. The README.md file explains how to generate data instances."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def _read_image_line(t):\n",
    "    # Each line of the file starts with 32 * 32 float values (the pixels), followed by an integer indicating the shape type (rectangle, ellipse, triangle).\n",
    "    s = t.split()\n",
    "    one_hot = np.zeros(NR_SHAPES, np.float32)\n",
    "    one_hot[int(s[1024])] = 1.0\n",
    "    # Pixel values are normalized to the range [-1, 1].\n",
    "    return [np.array([(float(x)-.5)*2.0 for x in s[:1024]], np.float32), one_hot]\n",
    "\n",
    "# Build a dataset from the selected files\n",
    "dataset = tf.data.Dataset.from_tensor_slices(\n",
    "    FILENAMES\n",
    ").flat_map(\n",
    "    lambda filename: (\n",
    "        # Each file should be ready line by line\n",
    "        tf.data.TextLineDataset(filename)\n",
    "        # Ignore lines starting with a hash sign\n",
    "        .filter(lambda line: tf.not_equal(tf.substr(line, 0, 1), \"#\"))\n",
    "        # If SHAPE_FILTER is set, only include shapes with the given ID\n",
    "        .filter(lambda line: True if SHAPE_FILTER is None else tf.equal(tf.string_split([line], \" \").values[1024], SHAPE_FILTER)) \n",
    "    )\n",
    ").map(\n",
    "    lambda t: tf.py_func(_read_image_line, [t], [np.float32, np.float32])\n",
    ").repeat(REPEATS).shuffle(buffer_size=20000).batch(BATCH_SIZE) # Allows to go through the dataset 100 times, records are batched.\n",
    "\n",
    "\n",
    "def get_input_tensors():\n",
    "    iterator = dataset.make_one_shot_iterator()\n",
    "    input_images, input_label = iterator.get_next()\n",
    "    input_images.set_shape([BATCH_SIZE, 1024])\n",
    "    reshaped_input_images = tf.reshape(input_images, [BATCH_SIZE, 32, 32])\n",
    "    images = tf.reshape(\n",
    "        tf.expand_dims(reshaped_input_images, -1),\n",
    "        [BATCH_SIZE, 32, 32, 1]\n",
    "    )\n",
    "    input_label.set_shape([BATCH_SIZE, NR_SHAPES])\n",
    "    return images, input_label"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sanity check that we're getting images.\n",
    "tf.reset_default_graph()\n",
    "images, _ = get_input_tensors()\n",
    "imgs_to_visualize = tfgan.eval.image_reshaper(images[:20,...], num_cols=10)\n",
    "visualize_images(imgs_to_visualize)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1><a id='unconditional_gan'></a>Unconditional GAN Example</h1>\n",
    "\n",
    "With unconditional GANs, we want a generator network to produce realistic-looking shapes. During training, the generator tries to produce realistic-enough shapes to 'fool' a discriminator network, while the discriminator tries to distinguish real shapes from generated ones. See the paper ['NIPS 2016 Tutorial: Generative Adversarial Networks'](https://arxiv.org/pdf/1701.00160.pdf) by Goodfellow or ['Generative Adversarial Networks'](https://arxiv.org/abs/1406.2661) by Goodfellow et al. for more details.\n",
    "\n",
    "The steps to using TFGAN to set up an unconditional GAN, in the simplest case, are as follows:\n",
    "\n",
    "1. **Model**: Set up the generator and discriminator graphs with a [`GANModel`](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/gan/python/namedtuples.py#L39) tuple. Use [`tfgan.gan_model`](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/gan/python/train.py#L64) or create one manually.\n",
    "1. **Losses**: Set up the generator and discriminator losses with a [`GANLoss`](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/gan/python/namedtuples.py#L115) tuple. Use [`tfgan.gan_loss`](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/gan/python/train.py#L328) or create one manually.\n",
    "1. **Train ops**: Set up TensorFlow ops that compute the loss, calculate the gradients, and update the weights with a [`GANTrainOps`](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/gan/python/namedtuples.py#L128) tuple. Use [`tfgan.gan_train_ops`](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/gan/python/train.py#L476) or create one manually.\n",
    "1. **Run alternating train loop**: Run the training Ops. This can be done with [`tfgan.gan_train`](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/gan/python/train.py#L661), or done manually.\n",
    "\n",
    "Each step can be performed by a TFGAN library call, or can be constructed manually for more control."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h2><a id='unconditional_model'></a>Model</h2>\n",
    "\n",
    "Set up a [GANModel tuple](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/gan/python/namedtuples.py#L39), which defines everything we need to perform GAN training. You can create the tuple with the library functions, or you can manually create one.\n",
    "\n",
    "Define the GANModel tuple using the TFGAN library function.\n",
    "For the simplest case, we need the following:\n",
    "\n",
    "1. A generator function that takes input noise and outputs generated MNIST digits\n",
    "\n",
    "1. A discriminator function that takes images and outputs a probability of  being real or fake\n",
    "\n",
    "1. Real images\n",
    "\n",
    "1. A noise vector to pass to the generator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3><a id=\"unconditional_generator\"></a>Generator</h3>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def generator_fn(noise, weight_decay=2.5e-5):\n",
    "    \"\"\"Simple generator to produce BabyShape images.\n",
    "    \n",
    "    Args:\n",
    "        noise: A single Tensor representing noise.\n",
    "        weight_decay: The value of the l2 weight decay.\n",
    "    \n",
    "    Returns:\n",
    "        A generated image in the range [-1, 1].\n",
    "    \"\"\"\n",
    "    with slim.arg_scope(\n",
    "        [layers.fully_connected, layers.conv2d_transpose],\n",
    "        activation_fn=tf.nn.relu, normalizer_fn=layers.batch_norm,\n",
    "        weights_regularizer=layers.l2_regularizer(weight_decay)):\n",
    "        net = layers.fully_connected(noise, 1024)\n",
    "        net = layers.fully_connected(net, 8 * 8 * 256)\n",
    "        net = tf.reshape(net, [-1, 8, 8, 256])\n",
    "        net = layers.conv2d_transpose(net, 64, [4, 4], stride=2)\n",
    "        net = layers.conv2d_transpose(net, 32, [4, 4], stride=2)\n",
    "        # Make sure that generator output is in the same range as `inputs`\n",
    "        # ie [-1, 1].\n",
    "        net = layers.conv2d(net, 1, 4, normalizer_fn=None, activation_fn=tf.tanh)\n",
    "\n",
    "        return net"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3><a id=\"unconditional_discriminator\"></a>Discriminator</h3>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def discriminator_fn(img, unused_conditioning, weight_decay=2.5e-5):\n",
    "    \"\"\"Discriminator network on MNIST digits.\n",
    "    \n",
    "    Args:\n",
    "        img: Real or generated MNIST digits. Should be in the range [-1, 1].\n",
    "        unused_conditioning: The TFGAN API can help with conditional GANs, which\n",
    "            would require extra `condition` information to both the generator and the\n",
    "            discriminator. Since this example is not conditional, we do not use this\n",
    "            argument.\n",
    "        weight_decay: The L2 weight decay.\n",
    "    \n",
    "    Returns:\n",
    "        Logits for the probability that the image is real.\n",
    "    \"\"\"\n",
    "    with slim.arg_scope(\n",
    "        [layers.conv2d, layers.fully_connected],\n",
    "        activation_fn=leaky_relu, normalizer_fn=None,\n",
    "        weights_regularizer=layers.l2_regularizer(weight_decay),\n",
    "        biases_regularizer=layers.l2_regularizer(weight_decay)):\n",
    "\n",
    "        net = layers.conv2d(img, 64, [4, 4], stride=2)\n",
    "        net = layers.conv2d(net, 128, [4, 4], stride=2)\n",
    "        net = layers.flatten(net)\n",
    "        net = layers.fully_connected(net, 1024, normalizer_fn=layers.batch_norm)\n",
    "        return layers.linear(net, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3><a id=\"unconditional_tuple\"></a>GANModel Tuple</h3>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tf.reset_default_graph()\n",
    "images, _ = get_input_tensors()\n",
    "gan_model = tfgan.gan_model(\n",
    "    generator_fn,\n",
    "    discriminator_fn,\n",
    "    real_data=images,\n",
    "    generator_inputs=tf.random_normal([BATCH_SIZE, NR_NOISE_DIMENSIONS]))\n",
    "\n",
    "# Sanity check that generated images before training are garbage.\n",
    "generated_data_to_visualize = tfgan.eval.image_reshaper(\n",
    "    gan_model.generated_data[:20,...], num_cols=10)\n",
    "visualize_images(generated_data_to_visualize)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h2><a id='unconditional_losses'></a>Losses</h2>\n",
    "\n",
    "We next set up the GAN model losses.\n",
    "\n",
    "Loss functions are currently an active area of research. The [losses library](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/gan/python/losses/python/losses_impl.py) provides some well-known or successful loss functions, such as the [original minimax](https://arxiv.org/abs/1406.2661), [Wasserstein](https://arxiv.org/abs/1701.07875) (by Arjovsky et al), and [improved Wasserstein](https://arxiv.org/abs/1704.00028) (by Gulrajani et al) losses. It is easy to add loss functions to the library as they are developed, and you can also pass in a custom loss function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We can use the minimax loss from the original paper.\n",
    "vanilla_gan_loss = tfgan.gan_loss(\n",
    "    gan_model,\n",
    "    generator_loss_fn=tfgan.losses.minimax_generator_loss,\n",
    "    discriminator_loss_fn=tfgan.losses.minimax_discriminator_loss)\n",
    "\n",
    "# We can use the Wasserstein loss (https://arxiv.org/abs/1701.07875) with the \n",
    "# gradient penalty from the improved Wasserstein loss paper \n",
    "# (https://arxiv.org/abs/1704.00028).\n",
    "improved_wgan_loss = tfgan.gan_loss(\n",
    "    gan_model,\n",
    "    # We make the loss explicit for demonstration, even though the default is \n",
    "    # Wasserstein loss.\n",
    "    generator_loss_fn=tfgan.losses.wasserstein_generator_loss,\n",
    "    discriminator_loss_fn=tfgan.losses.wasserstein_discriminator_loss,\n",
    "    gradient_penalty_weight=1.0)\n",
    "\n",
    "# We can also define custom losses to use with the rest of the TFGAN framework.\n",
    "def silly_custom_generator_loss(gan_model, add_summaries=False):\n",
    "    return tf.reduce_mean(gan_model.discriminator_gen_outputs)\n",
    "def silly_custom_discriminator_loss(gan_model, add_summaries=False):\n",
    "    return (tf.reduce_mean(gan_model.discriminator_gen_outputs) -\n",
    "            tf.reduce_mean(gan_model.discriminator_real_outputs))\n",
    "custom_gan_loss = tfgan.gan_loss(\n",
    "    gan_model,\n",
    "    generator_loss_fn=silly_custom_generator_loss,\n",
    "    discriminator_loss_fn=silly_custom_discriminator_loss)\n",
    "\n",
    "# Sanity check that we can evaluate our losses.\n",
    "for gan_loss, name in [(vanilla_gan_loss, 'vanilla loss'), \n",
    "                       (improved_wgan_loss, 'improved wgan loss'), \n",
    "                       (custom_gan_loss, 'custom loss')]:\n",
    "    evaluate_tfgan_loss(gan_loss, name)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h2><a id='unconditional_train'></a>Training and Evaluation</h2>\n",
    "\n",
    "<h3><a id='unconditional_train_ops'></a>Train Ops</h3>\n",
    "In order to train a GAN, we need to train both generator and discriminator networks using some variant of the alternating training paradigm. To do this, we construct a [GANTrainOps tuple](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/gan/python/namedtuples.py#L128) either manually or with a library call. We pass it the optimizers that we want to use, as well as any extra arguments that we'd like passed to slim's `create_train_op` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "generator_optimizer = tf.train.AdamOptimizer(0.001, beta1=0.5)\n",
    "discriminator_optimizer = tf.train.AdamOptimizer(0.0001, beta1=0.5)\n",
    "gan_train_ops = tfgan.gan_train_ops(\n",
    "    gan_model,\n",
    "    improved_wgan_loss,\n",
    "    generator_optimizer,\n",
    "    discriminator_optimizer)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3><a id=\"unconditional_train_steps\"></a>Train Steps</h3>\n",
    "\n",
    "Now we're ready to train. TFGAN handles the alternating training scheme that arises from the GAN minmax game. It also gives you the option of changing the ratio of discriminator updates to generator updates. Most applications (distributed setting, borg, etc) will use the [`gan_train`](https://github.com/tensorflow/tensorflow/blob/master/tensorflow/contrib/gan/python/train.py#L661) function, but we will use a different TFGAN utility and manually run the train ops so we can introspect more.\n",
    "\n",
    "This code block should take about **1 minute** to run on a GPU kernel, and about **5 minutes** on CPU."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We have the option to train the discriminator more than one step for every \n",
    "# step of the generator. In order to do this, we use a `GANTrainSteps` with \n",
    "# desired values. For this example, we use the default 1 generator train step \n",
    "# for every discriminator train step.\n",
    "train_step_fn = tfgan.get_sequential_train_steps(tfgan.GANTrainSteps(GENERATOR_STEPS, DISCRIMINATOR_STEPS))\n",
    "\n",
    "global_step = tf.train.get_or_create_global_step()\n",
    "loss_values, mnist_score_values  = [], []\n",
    "\n",
    "with tf.Session() as sess:\n",
    "    sess.run(tf.global_variables_initializer())\n",
    "    with slim.queues.QueueRunners(sess):\n",
    "        start_time = time.time()\n",
    "        i = 0\n",
    "        while True:\n",
    "            i += 1\n",
    "            try:\n",
    "                cur_loss, _ = train_step_fn(\n",
    "                    sess, gan_train_ops, global_step, train_step_kwargs={})\n",
    "            except (tf.errors.OutOfRangeError, tf.errors.InvalidArgumentError):\n",
    "                print(\"Done training\")\n",
    "                break\n",
    "            loss_values.append((i, cur_loss))\n",
    "            if i % 100 == 0:\n",
    "                print('Current loss: %f' % cur_loss)\n",
    "                visualize_training_generator(\n",
    "                    i, start_time, sess.run(generated_data_to_visualize))\n",
    "    print(\"results: \")\n",
    "    visualize_training_generator(i, start_time, sess.run(generated_data_to_visualize))\n",
    "print(\"Original images: {}\".format(DATASET_TYPE))\n",
    "imgs_to_visualize = tfgan.eval.image_reshaper(images[:20,...], num_cols=10)\n",
    "visualize_images(imgs_to_visualize)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the eval metrics over time.\n",
    "plt.title('Training loss per step')\n",
    "plt.plot(*zip(*loss_values))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h1><a id='conditional_gan'></a>Conditional GAN Example</h1>\n",
    "\n",
    "In the conditional GAN setting on BabyAIShapes, we wish to train a generator to produce\n",
    "realistic-looking shapes **of a particular type**. For example, we want to be able to produce as many squares as we want without producing other shapes. In contrast, in the unconditional case, we have no control over what shape the generator produces. In order to train a conditional generator, we pass the shape's identity to the generator and discriminator in addition to the noise vector. See [Conditional Generative Adversarial Nets](https://arxiv.org/abs/1411.1784) by Mirza and Osindero for more details."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h2><a id='conditional_model'></a>Model</h2>\n",
    "\n",
    "We perform the same procedure as in the unconditional case, but pass the shape label to the generator and discriminator as well as a random noise vector."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3><a id='conditional_generator'></a>Generator</h3>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def conditional_generator_fn(inputs, weight_decay=2.5e-5):\n",
    "    \"\"\"Generator to produce shape images.\n",
    "    \n",
    "    Args:\n",
    "        inputs: A 2-tuple of Tensors (noise, one_hot_labels).\n",
    "        weight_decay: The value of the l2 weight decay.\n",
    "\n",
    "    Returns:\n",
    "        A generated image in the range [-1, 1].\n",
    "    \"\"\"\n",
    "    noise, one_hot_labels = inputs\n",
    "  \n",
    "    with slim.arg_scope(\n",
    "        [layers.fully_connected, layers.conv2d_transpose],\n",
    "        activation_fn=tf.nn.relu, normalizer_fn=layers.batch_norm,\n",
    "        weights_regularizer=layers.l2_regularizer(weight_decay)):\n",
    "        net = layers.fully_connected(noise, 1024)\n",
    "        net = tfgan.features.condition_tensor_from_onehot(net, one_hot_labels)\n",
    "        net = layers.fully_connected(net, 8 * 8 * 128)\n",
    "        net = tf.reshape(net, [-1, 8, 8, 128])\n",
    "        net = layers.conv2d_transpose(net, 64, [4, 4], stride=2)\n",
    "        net = layers.conv2d_transpose(net, 32, [4, 4], stride=2)\n",
    "        # Make sure that generator output is in the same range as `inputs`\n",
    "        # ie [-1, 1].\n",
    "        net = layers.conv2d(net, 1, 4, normalizer_fn=None, activation_fn=tf.tanh)\n",
    "\n",
    "        return net"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3><a id='conditional_discriminator'></a>Discriminator</h3>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def conditional_discriminator_fn(img, conditioning, weight_decay=2.5e-5):\n",
    "    \"\"\"Conditional discriminator network on shapes.\n",
    "    \n",
    "    Args:\n",
    "        img: Real or generated shapes. Should be in the range [-1, 1].\n",
    "        conditioning: A 2-tuple of Tensors representing (noise, one_hot_labels).\n",
    "        weight_decay: The L2 weight decay.\n",
    "\n",
    "    Returns:\n",
    "        Logits for the probability that the image is real.\n",
    "    \"\"\"\n",
    "    _, one_hot_labels = conditioning\n",
    "    with slim.arg_scope(\n",
    "        [layers.conv2d, layers.fully_connected],\n",
    "        activation_fn=leaky_relu, normalizer_fn=None,\n",
    "        weights_regularizer=layers.l2_regularizer(weight_decay),\n",
    "        biases_regularizer=layers.l2_regularizer(weight_decay)):\n",
    "        net = layers.conv2d(img, 64, [4, 4], stride=2)\n",
    "        net = layers.conv2d(net, 128, [4, 4], stride=2)\n",
    "        net = layers.flatten(net)\n",
    "        net = tfgan.features.condition_tensor_from_onehot(net, one_hot_labels)\n",
    "        net = layers.fully_connected(net, 1024, normalizer_fn=layers.batch_norm)\n",
    "        \n",
    "        return layers.linear(net, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3><a id='conditional_tuple'></a>GANModel Tuple</h3>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tf.reset_default_graph()\n",
    "images, input_label = get_input_tensors()\n",
    "\n",
    "conditional_gan_model = tfgan.gan_model(\n",
    "    generator_fn=conditional_generator_fn,\n",
    "    discriminator_fn=conditional_discriminator_fn,\n",
    "    real_data=images,\n",
    "    generator_inputs=(tf.random_normal([BATCH_SIZE, NR_NOISE_DIMENSIONS]), \n",
    "                      input_label))\n",
    "\n",
    "# Sanity check that currently generated images are garbage.\n",
    "cond_generated_data_to_visualize = tfgan.eval.image_reshaper(\n",
    "    conditional_gan_model.generated_data[:20,...], num_cols=10)\n",
    "visualize_images(cond_generated_data_to_visualize)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h2><a id='conditional_losses'></a>Losses</h2>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "gan_loss = tfgan.gan_loss(\n",
    "    conditional_gan_model, gradient_penalty_weight=1.0)\n",
    "\n",
    "# Sanity check that we can evaluate our losses.\n",
    "evaluate_tfgan_loss(gan_loss)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h2><a id='conditional_train'></a>Training and Evaluation</h2>\n",
    "\n",
    "\n",
    "We use a slightly different learning rate schedule that involves decay."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3><a id='conditional_train_ops'></a>Train Ops</h2>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "generator_optimizer = tf.train.AdamOptimizer(0.0009, beta1=0.5)\n",
    "discriminator_optimizer = tf.train.AdamOptimizer(0.00009, beta1=0.5)\n",
    "gan_train_ops = tfgan.gan_train_ops(\n",
    "    conditional_gan_model,\n",
    "    gan_loss,\n",
    "    generator_optimizer,\n",
    "    discriminator_optimizer)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3><a id='conditional_train_evaluation'></a>Evaluation</h3>\n",
    "\n",
    "Since quantitative metrics for generators are sometimes tricky (see [A note on the evaluation of generative models](https://arxiv.org/abs/1511.01844) for some surprising ones), we also want to visualize our progress."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set up class-conditional visualization. We feed class labels to the generator\n",
    "# so that the the first column is `0`, the second column is `1`, etc.\n",
    "images_to_eval = 501\n",
    "assert images_to_eval % NR_SHAPES == 0\n",
    "\n",
    "random_noise = tf.random_normal([images_to_eval, NR_NOISE_DIMENSIONS])\n",
    "one_hot_labels = tf.one_hot(\n",
    "    [i for _ in range(images_to_eval // NR_SHAPES) for i in range(NR_SHAPES)], depth=NR_SHAPES) \n",
    "with tf.variable_scope(conditional_gan_model.generator_scope, reuse=True):\n",
    "    eval_images = conditional_gan_model.generator_fn(\n",
    "        (random_noise, one_hot_labels))\n",
    "reshaped_eval_imgs = tfgan.eval.image_reshaper(\n",
    "    eval_images[:7*NR_SHAPES, ...], num_cols=NR_SHAPES)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<h3><a id='conditional_train_steps'></a>Train Steps</h3>\n",
    "\n",
    "In this example, we train the generator and discriminator while keeping track of\n",
    "our important metric, the cross entropy loss with the real labels.\n",
    "\n",
    "This code block should take about **2 minutes** on GPU and **10 minutes** on CPU."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "global_step = tf.train.get_or_create_global_step()\n",
    "train_step_fn = tfgan.get_sequential_train_steps(tfgan.GANTrainSteps(GENERATOR_STEPS, DISCRIMINATOR_STEPS))\n",
    "loss_values, xent_score_values  = [], []\n",
    "\n",
    "with tf.Session() as sess:\n",
    "    sess.run(tf.global_variables_initializer())\n",
    "    with slim.queues.QueueRunners(sess):\n",
    "        start_time = time.time()\n",
    "        i = 0\n",
    "        while True:\n",
    "            i += 1\n",
    "            try:\n",
    "                cur_loss, _ = train_step_fn(\n",
    "                    sess, gan_train_ops, global_step, train_step_kwargs={})\n",
    "            except (tf.errors.OutOfRangeError, tf.errors.InvalidArgumentError):\n",
    "                print(ex)\n",
    "                break\n",
    "            loss_values.append((i, cur_loss))\n",
    "            if i % 100 == 0:\n",
    "                print('Current loss: %f' % cur_loss)\n",
    "                visualize_training_generator(i, start_time, sess.run(reshaped_eval_imgs))\n",
    "    print(\"results: \")\n",
    "    visualize_training_generator(i, start_time, sess.run(reshaped_eval_imgs))\n",
    "print(\"Original images: {}\".format(DATASET_TYPE))\n",
    "imgs_to_visualize = tfgan.eval.image_reshaper(images[:20,...], num_cols=10)\n",
    "visualize_images(imgs_to_visualize)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the eval metrics over time.\n",
    "plt.title('Training loss per step')\n",
    "plt.plot(*zip(*loss_values))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "celltoolbar": "Edit Metadata",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
